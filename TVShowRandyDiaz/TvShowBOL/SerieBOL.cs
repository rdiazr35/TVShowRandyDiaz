﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TvShowDAL;

namespace TvShowBOL
{
    public class SerieBOL
    {
        public TvShowDAL.SerieDAL tvShowDAL;
        /// <summary>
        /// Constructor
        /// </summary>
        public SerieBOL()
        {
            tvShowDAL = new SerieDAL();
        }
        /// <summary>
        /// CARGADO DE FAVORITOS
        /// </summary>
        /// <param name="id_usuario"></param>
        /// <returns></returns>
        public DataSet CargarFavoritos(string id_usuario = "1")
        {
            return tvShowDAL.CargarFavoritos(id_usuario);
        }
        /// <summary>
        /// CARGADO DE EPISODIOS VISTOS
        /// </summary>
        /// <param name="id_user"></param>
        /// <param name="id_serie"></param>
        /// <param name="id_season"></param>
        /// <returns></returns>
        public DataSet CargarEpisodesVistos(string id_user, string id_serie, string id_season)
        {
            return tvShowDAL.CargarEpisodesVistos(id_user, id_serie, id_season);
        }
        /// <summary>
        /// AGREGAR SERIES
        /// </summary>
        /// <param name="drSeleccionados"></param>
        public void Agregar(DataRow[] drSeleccionados)
        {
            foreach (DataRow pRow in drSeleccionados)
            {
                tvShowDAL.Agregar(pRow["id"].ToString(), pRow["name"].ToString(), pRow["language"].ToString(), 
                    pRow["type"].ToString(), pRow["image"].ToString(), pRow["status"].ToString(), pRow["rating"].ToString());
            }
        }
        /// <summary>
        /// AGREGAR EPISODIOS VISTOS
        /// </summary>
        /// <param name="drSeleccionados"></param>
        /// <param name="id_season"></param>
        /// <param name="id_serie"></param>
        public void AgregarEpisodes(DataRow[] drSeleccionados, string id_season, string id_serie)
        {
            foreach (DataRow pRow in drSeleccionados)
            {
                tvShowDAL.AgregarEpisodes("1", id_season, id_serie, pRow["id"].ToString());
            }
        }
    }
}
