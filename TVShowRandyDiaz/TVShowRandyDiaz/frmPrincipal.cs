﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TVShowRandyDiaz
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void btnBuscarSeriesPorNombre_Click(object sender, EventArgs e)
        {
            frmBuscasSeriesNombre frmBuscasSeriesNombre = new frmBuscasSeriesNombre();
            frmBuscasSeriesNombre.ShowDialog();
        }
    }
}
