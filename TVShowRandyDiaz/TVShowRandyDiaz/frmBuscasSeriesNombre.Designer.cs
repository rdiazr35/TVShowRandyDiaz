﻿namespace TVShowRandyDiaz
{
    partial class frmBuscasSeriesNombre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtgConsulta = new DevExpress.XtraGrid.GridControl();
            this.viewConsulta = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFavorita = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repFavorita = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLanguage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.edtRating = new DevExpress.XtraEditors.TextEdit();
            this.lblRating = new DevExpress.XtraEditors.LabelControl();
            this.edtStatus = new DevExpress.XtraEditors.TextEdit();
            this.lblStatus = new DevExpress.XtraEditors.LabelControl();
            this.ptImagen = new System.Windows.Forms.PictureBox();
            this.edtLenguage = new DevExpress.XtraEditors.TextEdit();
            this.Languaje = new DevExpress.XtraEditors.LabelControl();
            this.edtType = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.edtName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.edtId = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabSeries = new DevExpress.XtraTab.XtraTabPage();
            this.tabFavoritas = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.dtgFavoritas = new DevExpress.XtraGrid.GridControl();
            this.viewFavoritas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btnFiltroFavoritas = new DevExpress.XtraEditors.SimpleButton();
            this.dtgCapitulos = new DevExpress.XtraGrid.GridControl();
            this.viewCapitulos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.btnSalvarVisto = new DevExpress.XtraEditors.SimpleButton();
            this.dtgTemporada = new DevExpress.XtraGrid.GridControl();
            this.viewTemporada = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.btnFiltrar = new DevExpress.XtraEditors.SimpleButton();
            this.edtNameFiltro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tlbarMenu = new System.Windows.Forms.ToolStrip();
            this.btnAgregar = new System.Windows.Forms.ToolStripButton();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFavorita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtRating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtLenguage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabSeries.SuspendLayout();
            this.tabFavoritas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgFavoritas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewFavoritas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCapitulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewCapitulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTemporada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTemporada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtNameFiltro.Properties)).BeginInit();
            this.tlbarMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgConsulta
            // 
            this.dtgConsulta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgConsulta.Location = new System.Drawing.Point(0, 0);
            this.dtgConsulta.LookAndFeel.SkinName = "Black";
            this.dtgConsulta.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtgConsulta.MainView = this.viewConsulta;
            this.dtgConsulta.Name = "dtgConsulta";
            this.dtgConsulta.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repFavorita});
            this.dtgConsulta.Size = new System.Drawing.Size(337, 401);
            this.dtgConsulta.TabIndex = 0;
            this.dtgConsulta.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewConsulta});
            // 
            // viewConsulta
            // 
            this.viewConsulta.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.viewConsulta.Appearance.EvenRow.Options.UseBackColor = true;
            this.viewConsulta.Appearance.FixedLine.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewConsulta.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewConsulta.Appearance.FocusedCell.Options.UseFont = true;
            this.viewConsulta.Appearance.FocusedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewConsulta.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SteelBlue;
            this.viewConsulta.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewConsulta.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.viewConsulta.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewConsulta.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewConsulta.Appearance.FocusedRow.Options.UseFont = true;
            this.viewConsulta.Appearance.FocusedRow.Options.UseForeColor = true;
            this.viewConsulta.Appearance.GroupPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.viewConsulta.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightBlue;
            this.viewConsulta.Appearance.GroupPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewConsulta.Appearance.GroupPanel.Options.UseBackColor = true;
            this.viewConsulta.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewConsulta.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewConsulta.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.viewConsulta.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.viewConsulta.Appearance.OddRow.Options.UseBackColor = true;
            this.viewConsulta.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewConsulta.Appearance.SelectedRow.Options.UseBackColor = true;
            this.viewConsulta.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.viewConsulta.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFavorita,
            this.colId,
            this.colName,
            this.colLanguage,
            this.colType,
            this.colImage,
            this.gridColumn1,
            this.gridColumn2});
            this.viewConsulta.GridControl = this.dtgConsulta;
            this.viewConsulta.Name = "viewConsulta";
            this.viewConsulta.OptionsBehavior.AllowIncrementalSearch = true;
            this.viewConsulta.OptionsDetail.EnableMasterViewMode = false;
            this.viewConsulta.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.viewConsulta.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.viewConsulta.OptionsFilter.UseNewCustomFilterDialog = true;
            this.viewConsulta.OptionsLayout.Columns.StoreAllOptions = true;
            this.viewConsulta.OptionsLayout.Columns.StoreAppearance = true;
            this.viewConsulta.OptionsLayout.Columns.StoreLayout = false;
            this.viewConsulta.OptionsLayout.StoreAllOptions = true;
            this.viewConsulta.OptionsLayout.StoreAppearance = true;
            this.viewConsulta.OptionsPrint.AutoWidth = false;
            this.viewConsulta.OptionsPrint.ExpandAllGroups = false;
            this.viewConsulta.OptionsPrint.PrintDetails = true;
            this.viewConsulta.OptionsPrint.PrintGroupFooter = false;
            this.viewConsulta.OptionsView.ColumnAutoWidth = false;
            this.viewConsulta.OptionsView.EnableAppearanceEvenRow = true;
            this.viewConsulta.OptionsView.EnableAppearanceOddRow = true;
            this.viewConsulta.OptionsView.ShowAutoFilterRow = true;
            this.viewConsulta.PaintStyleName = "Skin";
            this.viewConsulta.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.viewConsulta_FocusedRowChanged);
            // 
            // colFavorita
            // 
            this.colFavorita.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colFavorita.AppearanceHeader.Options.UseFont = true;
            this.colFavorita.AppearanceHeader.Options.UseTextOptions = true;
            this.colFavorita.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFavorita.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFavorita.Caption = "Favorita";
            this.colFavorita.ColumnEdit = this.repFavorita;
            this.colFavorita.FieldName = "favorita";
            this.colFavorita.Name = "colFavorita";
            this.colFavorita.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colFavorita.Visible = true;
            this.colFavorita.VisibleIndex = 0;
            this.colFavorita.Width = 90;
            // 
            // repFavorita
            // 
            this.repFavorita.AutoHeight = false;
            this.repFavorita.DisplayValueChecked = "S";
            this.repFavorita.DisplayValueUnchecked = "N";
            this.repFavorita.Name = "repFavorita";
            this.repFavorita.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repFavorita.ValueChecked = "S";
            this.repFavorita.ValueGrayed = "";
            this.repFavorita.ValueUnchecked = "N";
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colId.AppearanceHeader.Options.UseFont = true;
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.ReadOnly = true;
            this.colId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colName.AppearanceHeader.Options.UseFont = true;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colName.Caption = "Name";
            this.colName.FieldName = "name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 212;
            // 
            // colLanguage
            // 
            this.colLanguage.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colLanguage.AppearanceHeader.Options.UseFont = true;
            this.colLanguage.AppearanceHeader.Options.UseTextOptions = true;
            this.colLanguage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLanguage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLanguage.Caption = "Language";
            this.colLanguage.FieldName = "language";
            this.colLanguage.Name = "colLanguage";
            this.colLanguage.OptionsColumn.ReadOnly = true;
            this.colLanguage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLanguage.Width = 200;
            // 
            // colType
            // 
            this.colType.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colType.AppearanceHeader.Options.UseFont = true;
            this.colType.AppearanceHeader.Options.UseTextOptions = true;
            this.colType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colType.Caption = "Type";
            this.colType.FieldName = "type";
            this.colType.Name = "colType";
            this.colType.OptionsColumn.ReadOnly = true;
            this.colType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colType.Width = 200;
            // 
            // colImage
            // 
            this.colImage.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colImage.AppearanceHeader.Options.UseFont = true;
            this.colImage.AppearanceHeader.Options.UseTextOptions = true;
            this.colImage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colImage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colImage.Caption = "Image";
            this.colImage.FieldName = "image";
            this.colImage.Name = "colImage";
            this.colImage.OptionsColumn.ReadOnly = true;
            this.colImage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colImage.Width = 200;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.Caption = "Status";
            this.gridColumn1.FieldName = "status";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn1.Width = 200;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.Caption = "Rating";
            this.gridColumn2.FieldName = "rating";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn2.Width = 200;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.edtRating);
            this.groupControl1.Controls.Add(this.lblRating);
            this.groupControl1.Controls.Add(this.edtStatus);
            this.groupControl1.Controls.Add(this.lblStatus);
            this.groupControl1.Controls.Add(this.ptImagen);
            this.groupControl1.Controls.Add(this.edtLenguage);
            this.groupControl1.Controls.Add(this.Languaje);
            this.groupControl1.Controls.Add(this.edtType);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.edtName);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.edtId);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Black";
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(738, 489);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Detalle";
            // 
            // edtRating
            // 
            this.edtRating.Location = new System.Drawing.Point(71, 136);
            this.edtRating.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtRating.Name = "edtRating";
            this.edtRating.Properties.LookAndFeel.SkinName = "Black";
            this.edtRating.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.edtRating.Properties.ReadOnly = true;
            this.edtRating.Size = new System.Drawing.Size(202, 20);
            this.edtRating.TabIndex = 14;
            // 
            // lblRating
            // 
            this.lblRating.Location = new System.Drawing.Point(14, 139);
            this.lblRating.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(35, 13);
            this.lblRating.TabIndex = 13;
            this.lblRating.Text = "Rating:";
            // 
            // edtStatus
            // 
            this.edtStatus.Location = new System.Drawing.Point(71, 115);
            this.edtStatus.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtStatus.Name = "edtStatus";
            this.edtStatus.Properties.LookAndFeel.SkinName = "Black";
            this.edtStatus.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.edtStatus.Properties.ReadOnly = true;
            this.edtStatus.Size = new System.Drawing.Size(202, 20);
            this.edtStatus.TabIndex = 12;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(14, 118);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(35, 13);
            this.lblStatus.TabIndex = 11;
            this.lblStatus.Text = "Status:";
            // 
            // ptImagen
            // 
            this.ptImagen.Location = new System.Drawing.Point(71, 160);
            this.ptImagen.Name = "ptImagen";
            this.ptImagen.Size = new System.Drawing.Size(202, 238);
            this.ptImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptImagen.TabIndex = 10;
            this.ptImagen.TabStop = false;
            // 
            // edtLenguage
            // 
            this.edtLenguage.Location = new System.Drawing.Point(71, 94);
            this.edtLenguage.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtLenguage.Name = "edtLenguage";
            this.edtLenguage.Properties.LookAndFeel.SkinName = "Black";
            this.edtLenguage.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.edtLenguage.Properties.ReadOnly = true;
            this.edtLenguage.Size = new System.Drawing.Size(202, 20);
            this.edtLenguage.TabIndex = 7;
            // 
            // Languaje
            // 
            this.Languaje.Location = new System.Drawing.Point(14, 97);
            this.Languaje.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.Languaje.Name = "Languaje";
            this.Languaje.Size = new System.Drawing.Size(51, 13);
            this.Languaje.TabIndex = 6;
            this.Languaje.Text = "Language:";
            // 
            // edtType
            // 
            this.edtType.Location = new System.Drawing.Point(71, 73);
            this.edtType.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtType.Name = "edtType";
            this.edtType.Properties.LookAndFeel.SkinName = "Black";
            this.edtType.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.edtType.Properties.ReadOnly = true;
            this.edtType.Size = new System.Drawing.Size(202, 20);
            this.edtType.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(14, 76);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Type:";
            // 
            // edtName
            // 
            this.edtName.Location = new System.Drawing.Point(71, 52);
            this.edtName.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtName.Name = "edtName";
            this.edtName.Properties.LookAndFeel.SkinName = "Black";
            this.edtName.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.edtName.Properties.ReadOnly = true;
            this.edtName.Size = new System.Drawing.Size(202, 20);
            this.edtName.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(14, 55);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Name:";
            // 
            // edtId
            // 
            this.edtId.Location = new System.Drawing.Point(71, 31);
            this.edtId.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtId.Name = "edtId";
            this.edtId.Properties.LookAndFeel.SkinName = "Black";
            this.edtId.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.edtId.Properties.ReadOnly = true;
            this.edtId.Size = new System.Drawing.Size(202, 20);
            this.edtId.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(14, 34);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(14, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Id:";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 99);
            this.splitContainerControl1.LookAndFeel.SkinName = "Black";
            this.splitContainerControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.dtgConsulta);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.tabControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1088, 401);
            this.splitContainerControl1.SplitterPosition = 337;
            this.splitContainerControl1.TabIndex = 3;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.LookAndFeel.SkinName = "Black";
            this.tabControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabSeries;
            this.tabControl.Size = new System.Drawing.Size(745, 401);
            this.tabControl.TabIndex = 1;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabSeries,
            this.tabFavoritas});
            // 
            // tabSeries
            // 
            this.tabSeries.Controls.Add(this.groupControl1);
            this.tabSeries.Name = "tabSeries";
            this.tabSeries.Size = new System.Drawing.Size(738, 489);
            this.tabSeries.Text = "Series";
            // 
            // tabFavoritas
            // 
            this.tabFavoritas.Controls.Add(this.splitContainerControl2);
            this.tabFavoritas.Name = "tabFavoritas";
            this.tabFavoritas.Size = new System.Drawing.Size(738, 373);
            this.tabFavoritas.Text = "Mis series favoritas";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.LookAndFeel.SkinName = "Black";
            this.splitContainerControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.dtgFavoritas);
            this.splitContainerControl2.Panel1.Controls.Add(this.groupControl3);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.dtgCapitulos);
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl4);
            this.splitContainerControl2.Panel2.Controls.Add(this.dtgTemporada);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(738, 373);
            this.splitContainerControl2.SplitterPosition = 255;
            this.splitContainerControl2.TabIndex = 3;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // dtgFavoritas
            // 
            this.dtgFavoritas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgFavoritas.Location = new System.Drawing.Point(0, 54);
            this.dtgFavoritas.LookAndFeel.SkinName = "Black";
            this.dtgFavoritas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtgFavoritas.MainView = this.viewFavoritas;
            this.dtgFavoritas.Name = "dtgFavoritas";
            this.dtgFavoritas.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.dtgFavoritas.Size = new System.Drawing.Size(255, 319);
            this.dtgFavoritas.TabIndex = 1;
            this.dtgFavoritas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewFavoritas});
            // 
            // viewFavoritas
            // 
            this.viewFavoritas.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.viewFavoritas.Appearance.EvenRow.Options.UseBackColor = true;
            this.viewFavoritas.Appearance.FixedLine.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewFavoritas.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewFavoritas.Appearance.FocusedCell.Options.UseFont = true;
            this.viewFavoritas.Appearance.FocusedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewFavoritas.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SteelBlue;
            this.viewFavoritas.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewFavoritas.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.viewFavoritas.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewFavoritas.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewFavoritas.Appearance.FocusedRow.Options.UseFont = true;
            this.viewFavoritas.Appearance.FocusedRow.Options.UseForeColor = true;
            this.viewFavoritas.Appearance.GroupPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.viewFavoritas.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightBlue;
            this.viewFavoritas.Appearance.GroupPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewFavoritas.Appearance.GroupPanel.Options.UseBackColor = true;
            this.viewFavoritas.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewFavoritas.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewFavoritas.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.viewFavoritas.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.viewFavoritas.Appearance.OddRow.Options.UseBackColor = true;
            this.viewFavoritas.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewFavoritas.Appearance.SelectedRow.Options.UseBackColor = true;
            this.viewFavoritas.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.viewFavoritas.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10});
            this.viewFavoritas.GridControl = this.dtgFavoritas;
            this.viewFavoritas.Name = "viewFavoritas";
            this.viewFavoritas.OptionsBehavior.AllowIncrementalSearch = true;
            this.viewFavoritas.OptionsBehavior.Editable = false;
            this.viewFavoritas.OptionsBehavior.ReadOnly = true;
            this.viewFavoritas.OptionsDetail.EnableMasterViewMode = false;
            this.viewFavoritas.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.viewFavoritas.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.viewFavoritas.OptionsFilter.UseNewCustomFilterDialog = true;
            this.viewFavoritas.OptionsLayout.Columns.StoreAllOptions = true;
            this.viewFavoritas.OptionsLayout.Columns.StoreAppearance = true;
            this.viewFavoritas.OptionsLayout.Columns.StoreLayout = false;
            this.viewFavoritas.OptionsLayout.StoreAllOptions = true;
            this.viewFavoritas.OptionsLayout.StoreAppearance = true;
            this.viewFavoritas.OptionsPrint.AutoWidth = false;
            this.viewFavoritas.OptionsPrint.ExpandAllGroups = false;
            this.viewFavoritas.OptionsPrint.PrintDetails = true;
            this.viewFavoritas.OptionsPrint.PrintGroupFooter = false;
            this.viewFavoritas.OptionsView.ColumnAutoWidth = false;
            this.viewFavoritas.OptionsView.EnableAppearanceEvenRow = true;
            this.viewFavoritas.OptionsView.EnableAppearanceOddRow = true;
            this.viewFavoritas.OptionsView.ShowAutoFilterRow = true;
            this.viewFavoritas.PaintStyleName = "Skin";
            this.viewFavoritas.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.viewFavoritas_FocusedRowChanged);
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.Caption = "Id";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.Caption = "Name";
            this.gridColumn5.FieldName = "name";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 212;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.Caption = "Language";
            this.gridColumn6.FieldName = "language";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 200;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn7.Caption = "Type";
            this.gridColumn7.FieldName = "type";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 200;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn8.Caption = "Image";
            this.gridColumn8.FieldName = "image";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn8.Width = 200;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.Caption = "Status";
            this.gridColumn9.FieldName = "status";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            this.gridColumn9.Width = 200;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.Caption = "Rating";
            this.gridColumn10.FieldName = "rating";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            this.gridColumn10.Width = 200;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.DisplayValueChecked = "S";
            this.repositoryItemCheckEdit1.DisplayValueUnchecked = "N";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ValueChecked = "S";
            this.repositoryItemCheckEdit1.ValueGrayed = "";
            this.repositoryItemCheckEdit1.ValueUnchecked = "N";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.btnFiltroFavoritas);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.LookAndFeel.SkinName = "Black";
            this.groupControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(255, 54);
            this.groupControl3.TabIndex = 2;
            this.groupControl3.Text = "Filtro";
            // 
            // btnFiltroFavoritas
            // 
            this.btnFiltroFavoritas.Location = new System.Drawing.Point(23, 26);
            this.btnFiltroFavoritas.LookAndFeel.SkinName = "Black";
            this.btnFiltroFavoritas.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnFiltroFavoritas.Name = "btnFiltroFavoritas";
            this.btnFiltroFavoritas.Size = new System.Drawing.Size(75, 23);
            this.btnFiltroFavoritas.TabIndex = 2;
            this.btnFiltroFavoritas.Text = "Filtrar";
            this.btnFiltroFavoritas.Click += new System.EventHandler(this.btnFiltroFavoritas_Click);
            // 
            // dtgCapitulos
            // 
            this.dtgCapitulos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgCapitulos.Location = new System.Drawing.Point(0, 280);
            this.dtgCapitulos.LookAndFeel.SkinName = "Black";
            this.dtgCapitulos.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtgCapitulos.MainView = this.viewCapitulos;
            this.dtgCapitulos.Name = "dtgCapitulos";
            this.dtgCapitulos.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3});
            this.dtgCapitulos.Size = new System.Drawing.Size(477, 93);
            this.dtgCapitulos.TabIndex = 3;
            this.dtgCapitulos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewCapitulos});
            // 
            // viewCapitulos
            // 
            this.viewCapitulos.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.viewCapitulos.Appearance.EvenRow.Options.UseBackColor = true;
            this.viewCapitulos.Appearance.FixedLine.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewCapitulos.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewCapitulos.Appearance.FocusedCell.Options.UseFont = true;
            this.viewCapitulos.Appearance.FocusedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewCapitulos.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SteelBlue;
            this.viewCapitulos.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewCapitulos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.viewCapitulos.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewCapitulos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewCapitulos.Appearance.FocusedRow.Options.UseFont = true;
            this.viewCapitulos.Appearance.FocusedRow.Options.UseForeColor = true;
            this.viewCapitulos.Appearance.GroupPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.viewCapitulos.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightBlue;
            this.viewCapitulos.Appearance.GroupPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewCapitulos.Appearance.GroupPanel.Options.UseBackColor = true;
            this.viewCapitulos.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewCapitulos.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewCapitulos.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.viewCapitulos.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.viewCapitulos.Appearance.OddRow.Options.UseBackColor = true;
            this.viewCapitulos.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewCapitulos.Appearance.SelectedRow.Options.UseBackColor = true;
            this.viewCapitulos.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.viewCapitulos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisto,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn12});
            this.viewCapitulos.GridControl = this.dtgCapitulos;
            this.viewCapitulos.Name = "viewCapitulos";
            this.viewCapitulos.OptionsBehavior.AllowIncrementalSearch = true;
            this.viewCapitulos.OptionsDetail.EnableMasterViewMode = false;
            this.viewCapitulos.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.viewCapitulos.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.viewCapitulos.OptionsFilter.UseNewCustomFilterDialog = true;
            this.viewCapitulos.OptionsLayout.Columns.StoreAllOptions = true;
            this.viewCapitulos.OptionsLayout.Columns.StoreAppearance = true;
            this.viewCapitulos.OptionsLayout.Columns.StoreLayout = false;
            this.viewCapitulos.OptionsLayout.StoreAllOptions = true;
            this.viewCapitulos.OptionsLayout.StoreAppearance = true;
            this.viewCapitulos.OptionsPrint.AutoWidth = false;
            this.viewCapitulos.OptionsPrint.ExpandAllGroups = false;
            this.viewCapitulos.OptionsPrint.PrintDetails = true;
            this.viewCapitulos.OptionsPrint.PrintGroupFooter = false;
            this.viewCapitulos.OptionsView.ColumnAutoWidth = false;
            this.viewCapitulos.OptionsView.EnableAppearanceEvenRow = true;
            this.viewCapitulos.OptionsView.EnableAppearanceOddRow = true;
            this.viewCapitulos.OptionsView.ShowAutoFilterRow = true;
            this.viewCapitulos.PaintStyleName = "Skin";
            this.viewCapitulos.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.viewCapitulos_FocusedRowChanged);
            this.viewCapitulos.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.viewCapitulos_CellValueChanged);
            this.viewCapitulos.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.viewCapitulos_CellValueChanging);
            // 
            // colVisto
            // 
            this.colVisto.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colVisto.AppearanceHeader.Options.UseFont = true;
            this.colVisto.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisto.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVisto.Caption = "Visto";
            this.colVisto.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colVisto.FieldName = "visto";
            this.colVisto.Name = "colVisto";
            this.colVisto.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colVisto.Visible = true;
            this.colVisto.VisibleIndex = 0;
            this.colVisto.Width = 100;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.DisplayValueChecked = "S";
            this.repositoryItemCheckEdit3.DisplayValueUnchecked = "N";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit3.ValueChecked = "S";
            this.repositoryItemCheckEdit3.ValueGrayed = "";
            this.repositoryItemCheckEdit3.ValueUnchecked = "N";
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.Caption = "Id";
            this.gridColumn17.FieldName = "id";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn18.AppearanceHeader.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn18.Caption = "Name";
            this.gridColumn18.FieldName = "name";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 3;
            this.gridColumn18.Width = 212;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn19.AppearanceHeader.Options.UseFont = true;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn19.Caption = "Season";
            this.gridColumn19.FieldName = "season";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 4;
            this.gridColumn19.Width = 87;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn20.AppearanceHeader.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn20.Caption = "Number";
            this.gridColumn20.FieldName = "number";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 2;
            this.gridColumn20.Width = 82;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn21.AppearanceHeader.Options.UseFont = true;
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn21.Caption = "Image";
            this.gridColumn21.FieldName = "image";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 5;
            this.gridColumn21.Width = 200;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.btnSalvarVisto);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl4.Location = new System.Drawing.Point(0, 226);
            this.groupControl4.LookAndFeel.SkinName = "Black";
            this.groupControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(477, 54);
            this.groupControl4.TabIndex = 4;
            this.groupControl4.Text = "Filtro";
            // 
            // btnSalvarVisto
            // 
            this.btnSalvarVisto.Location = new System.Drawing.Point(23, 26);
            this.btnSalvarVisto.LookAndFeel.SkinName = "Black";
            this.btnSalvarVisto.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSalvarVisto.Name = "btnSalvarVisto";
            this.btnSalvarVisto.Size = new System.Drawing.Size(75, 23);
            this.btnSalvarVisto.TabIndex = 2;
            this.btnSalvarVisto.Text = "Salvar";
            this.btnSalvarVisto.Click += new System.EventHandler(this.btnVisto_Click);
            // 
            // dtgTemporada
            // 
            this.dtgTemporada.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtgTemporada.Location = new System.Drawing.Point(0, 0);
            this.dtgTemporada.LookAndFeel.SkinName = "Black";
            this.dtgTemporada.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtgTemporada.MainView = this.viewTemporada;
            this.dtgTemporada.Name = "dtgTemporada";
            this.dtgTemporada.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.dtgTemporada.Size = new System.Drawing.Size(477, 226);
            this.dtgTemporada.TabIndex = 2;
            this.dtgTemporada.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewTemporada});
            // 
            // viewTemporada
            // 
            this.viewTemporada.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.viewTemporada.Appearance.EvenRow.Options.UseBackColor = true;
            this.viewTemporada.Appearance.FixedLine.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewTemporada.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewTemporada.Appearance.FocusedCell.Options.UseFont = true;
            this.viewTemporada.Appearance.FocusedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewTemporada.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SteelBlue;
            this.viewTemporada.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewTemporada.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.viewTemporada.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewTemporada.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewTemporada.Appearance.FocusedRow.Options.UseFont = true;
            this.viewTemporada.Appearance.FocusedRow.Options.UseForeColor = true;
            this.viewTemporada.Appearance.GroupPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.viewTemporada.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightBlue;
            this.viewTemporada.Appearance.GroupPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewTemporada.Appearance.GroupPanel.Options.UseBackColor = true;
            this.viewTemporada.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewTemporada.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewTemporada.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.viewTemporada.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.viewTemporada.Appearance.OddRow.Options.UseBackColor = true;
            this.viewTemporada.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewTemporada.Appearance.SelectedRow.Options.UseBackColor = true;
            this.viewTemporada.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.viewTemporada.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn11});
            this.viewTemporada.GridControl = this.dtgTemporada;
            this.viewTemporada.Name = "viewTemporada";
            this.viewTemporada.OptionsBehavior.AllowIncrementalSearch = true;
            this.viewTemporada.OptionsBehavior.Editable = false;
            this.viewTemporada.OptionsBehavior.ReadOnly = true;
            this.viewTemporada.OptionsDetail.EnableMasterViewMode = false;
            this.viewTemporada.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.viewTemporada.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.viewTemporada.OptionsFilter.UseNewCustomFilterDialog = true;
            this.viewTemporada.OptionsLayout.Columns.StoreAllOptions = true;
            this.viewTemporada.OptionsLayout.Columns.StoreAppearance = true;
            this.viewTemporada.OptionsLayout.Columns.StoreLayout = false;
            this.viewTemporada.OptionsLayout.StoreAllOptions = true;
            this.viewTemporada.OptionsLayout.StoreAppearance = true;
            this.viewTemporada.OptionsPrint.AutoWidth = false;
            this.viewTemporada.OptionsPrint.ExpandAllGroups = false;
            this.viewTemporada.OptionsPrint.PrintDetails = true;
            this.viewTemporada.OptionsPrint.PrintGroupFooter = false;
            this.viewTemporada.OptionsView.ColumnAutoWidth = false;
            this.viewTemporada.OptionsView.EnableAppearanceEvenRow = true;
            this.viewTemporada.OptionsView.EnableAppearanceOddRow = true;
            this.viewTemporada.OptionsView.ShowAutoFilterRow = true;
            this.viewTemporada.PaintStyleName = "Skin";
            this.viewTemporada.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.viewTemporada_FocusedRowChanged);
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.Caption = "Id";
            this.gridColumn3.FieldName = "id";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn13.Caption = "Number";
            this.gridColumn13.FieldName = "number";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 1;
            this.gridColumn13.Width = 95;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn14.Caption = "Image";
            this.gridColumn14.FieldName = "image";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 2;
            this.gridColumn14.Width = 221;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn11.Caption = "Name";
            this.gridColumn11.FieldName = "name";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 212;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.DisplayValueChecked = "S";
            this.repositoryItemCheckEdit2.DisplayValueUnchecked = "N";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit2.ValueChecked = "S";
            this.repositoryItemCheckEdit2.ValueGrayed = "";
            this.repositoryItemCheckEdit2.ValueUnchecked = "N";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.btnSalir);
            this.groupControl2.Controls.Add(this.btnLimpiar);
            this.groupControl2.Controls.Add(this.btnFiltrar);
            this.groupControl2.Controls.Add(this.edtNameFiltro);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 35);
            this.groupControl2.LookAndFeel.SkinName = "Black";
            this.groupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1088, 64);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Filtro";
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(481, 30);
            this.btnSalir.LookAndFeel.SkinName = "Black";
            this.btnSalir.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(400, 30);
            this.btnLimpiar.LookAndFeel.SkinName = "Black";
            this.btnLimpiar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 3;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Location = new System.Drawing.Point(319, 30);
            this.btnFiltrar.LookAndFeel.SkinName = "Black";
            this.btnFiltrar.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(75, 23);
            this.btnFiltrar.TabIndex = 2;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // edtNameFiltro
            // 
            this.edtNameFiltro.Location = new System.Drawing.Point(111, 31);
            this.edtNameFiltro.Name = "edtNameFiltro";
            this.edtNameFiltro.Size = new System.Drawing.Size(202, 20);
            this.edtNameFiltro.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(93, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Nombre de la serie:";
            // 
            // tlbarMenu
            // 
            this.tlbarMenu.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.tlbarMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAgregar});
            this.tlbarMenu.Location = new System.Drawing.Point(0, 0);
            this.tlbarMenu.Name = "tlbarMenu";
            this.tlbarMenu.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.tlbarMenu.Size = new System.Drawing.Size(1088, 35);
            this.tlbarMenu.TabIndex = 7;
            // 
            // btnAgregar
            // 
            this.btnAgregar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAgregar.Image = global::TVShowRandyDiaz.Properties.Resources._04;
            this.btnAgregar.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.btnAgregar.Size = new System.Drawing.Size(42, 32);
            this.btnAgregar.ToolTipText = "Add";
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "gridColumn12";
            this.gridColumn12.FieldName = "aux_bd";
            this.gridColumn12.Name = "gridColumn12";
            // 
            // frmBuscasSeriesNombre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 500);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.tlbarMenu);
            this.Name = "frmBuscasSeriesNombre";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Series por Nombre";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmBuscasSeriesNombre_Load);
            this.Shown += new System.EventHandler(this.frmBuscasSeriesNombre_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dtgConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFavorita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtRating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtLenguage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabSeries.ResumeLayout(false);
            this.tabFavoritas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgFavoritas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewFavoritas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgCapitulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewCapitulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTemporada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTemporada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtNameFiltro.Properties)).EndInit();
            this.tlbarMenu.ResumeLayout(false);
            this.tlbarMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl dtgConsulta;
        private DevExpress.XtraGrid.Views.Grid.GridView viewConsulta;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit edtNameFiltro;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnFiltrar;
        private DevExpress.XtraEditors.TextEdit edtId;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit edtName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit edtType;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit edtLenguage;
        private DevExpress.XtraEditors.LabelControl Languaje;
        private System.Windows.Forms.PictureBox ptImagen;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colLanguage;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colImage;
        private DevExpress.XtraEditors.TextEdit edtRating;
        private DevExpress.XtraEditors.LabelControl lblRating;
        private DevExpress.XtraEditors.TextEdit edtStatus;
        private DevExpress.XtraEditors.LabelControl lblStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private System.Windows.Forms.ToolStrip tlbarMenu;
        private System.Windows.Forms.ToolStripButton btnAgregar;
        private DevExpress.XtraGrid.Columns.GridColumn colFavorita;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repFavorita;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabSeries;
        private DevExpress.XtraTab.XtraTabPage tabFavoritas;
        private DevExpress.XtraGrid.GridControl dtgFavoritas;
        private DevExpress.XtraGrid.Views.Grid.GridView viewFavoritas;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton btnFiltroFavoritas;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl dtgCapitulos;
        private DevExpress.XtraGrid.Views.Grid.GridView viewCapitulos;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.GridControl dtgTemporada;
        private DevExpress.XtraGrid.Views.Grid.GridView viewTemporada;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisto;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton btnSalvarVisto;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
    }
}