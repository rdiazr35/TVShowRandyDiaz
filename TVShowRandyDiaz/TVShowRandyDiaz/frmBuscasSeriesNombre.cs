﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using TvShowENL;

namespace TVShowRandyDiaz
{
    public partial class frmBuscasSeriesNombre : Form
    {
        TvShowBOL.SerieBOL serieBOL;
        DataSet dsetSeries;
        DataSet dsetSeason;
        DataSet dsetEpisodes;
        DataRow[] drSeleccionados;
        public frmBuscasSeriesNombre()
        {
            InitializeComponent();
        }

        private void frmBuscasSeriesNombre_Load(object sender, EventArgs e)
        {
            serieBOL = new TvShowBOL.SerieBOL();
        }
        /// <summary>
        /// 
        /// </summary>
        internal void LoadSeries()
        {
            if (!this.edtNameFiltro.Text.Equals(""))
            {
                string sUrlRequest = "http://api.tvmaze.com/search/shows?q=" + this.edtNameFiltro.Text +" ";
                var json = new WebClient().DownloadString(sUrlRequest);
                JavaScriptSerializer ser = new JavaScriptSerializer();
                var a = ser.Deserialize<List<Class1>>(json);

                dsetSeries = new DataSet();
                DataTable dt = new DataTable();
                dt.TableName = "series";
                dt.Columns.Add("favorita");
                dt.Columns.Add("id");
                dt.Columns.Add("name");
                dt.Columns.Add("language");
                dt.Columns.Add("type");
                dt.Columns.Add("image");
                dt.Columns.Add("status");
                dt.Columns.Add("rating");
                foreach (var item in a)
                {
                    dt.Rows.Add("N",item.show.id, item.show.name, item.show.language, item.show.type, item.show.image.original, item.show.status, item.show.rating.average);
                }
                dsetSeries.Tables.Add(dt.Copy());
                this.dtgConsulta.DataSource = null;
                this.dtgConsulta.DataBindings.Clear();
                if (dsetSeries.Tables[0].Rows.Count.Equals(0))
                {
                    this.edtId.Text = "";
                    this.edtLenguage.Text = "";
                    this.edtName.Text = "";
                    this.edtType.Text = "";
                    this.edtStatus.Text = "";
                    this.edtRating.Text = "";
                    this.ptImagen.Image = null;
                }
                else
                    this.dtgConsulta.DataSource = dsetSeries.Tables[0];
                
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            LoadSeries();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            this.edtNameFiltro.Text = "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void viewConsulta_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (this.viewConsulta.FocusedRowHandle >= 0)
            {
                DataRow dr = viewConsulta.GetFocusedDataRow();
                this.edtId.Text = dr["id"].ToString();
                this.edtLenguage.Text = dr["language"].ToString();
                this.edtName.Text = dr["name"].ToString();
                this.edtType.Text = dr["type"].ToString();
                this.edtStatus.Text = dr["status"].ToString();
                this.edtRating.Text = dr["rating"].ToString();
                string img = dr["image"].ToString();
                this.ptImagen.Load(dr["image"].ToString());
            }
        }

        private void frmBuscasSeriesNombre_Shown(object sender, EventArgs e)
        {
            this.edtNameFiltro.Focus();
            this.edtNameFiltro.SelectAll();
        }

        private void btnFiltroFavoritas_Click(object sender, EventArgs e)
        {
            DataSet dataSet = serieBOL.CargarFavoritos("1");
            dtgFavoritas.DataSource = dataSet.Tables[0];
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                int indice = this.viewConsulta.FocusedRowHandle;
                this.viewConsulta.FocusedRowHandle = -1;
                this.viewConsulta.FocusedRowHandle = indice;
                if (this.viewConsulta.FocusedRowHandle >= 0)
                    this.drSeleccionados = this.dsetSeries.Tables[0].Select("favorita = 'S'");
            }
            catch { }
            try
            {
                if (this.drSeleccionados.Length == 0)
                {
                    MessageBox.Show(this, "Debe de seleccionar una serie antes de continuar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                serieBOL.Agregar(this.drSeleccionados);
                this.btnFiltrar_Click(null, null);
            }
            catch {

            }
        }

        private void viewFavoritas_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (this.viewFavoritas.FocusedRowHandle >= 0)
            {
                this.dtgCapitulos.DataSource = null;
                this.dtgTemporada.DataSource = null;
                DataRow dr = viewFavoritas.GetFocusedDataRow();
                string id = dr["id"].ToString();
                string sUrlRequest = "http://api.tvmaze.com/shows/"+ id + "/seasons";
                var json = new WebClient().DownloadString(sUrlRequest);
                JavaScriptSerializer ser = new JavaScriptSerializer();
                var a = ser.Deserialize<List<Class12>>(json);

                dsetSeason = new DataSet();
                DataTable dt = new DataTable();
                dt.TableName = "season";
                dt.Columns.Add("id");
                dt.Columns.Add("name");
                dt.Columns.Add("number");
                dt.Columns.Add("image");
                foreach (var item in a)
                {
                    string ruta = "";
                    if (item.image != null)
                    {
                        ruta = item.image.original;
                    }
                    dt.Rows.Add(item.id, item.name, item.number, ruta);
                }
                dsetSeason.Tables.Add(dt.Copy());
               
                this.dtgTemporada.DataSource = dsetSeason.Tables[0];
                this.viewTemporada.FocusedRowHandle = -1;
                this.viewTemporada.FocusedRowHandle = 0;
                //this.viewFavoritas.FocusedRowHandle = -1;
            }
        }

        private void viewCapitulos_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
           
        }

        private void viewTemporada_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (this.viewTemporada.FocusedRowHandle >= 0)
            {
                DataRow dr = viewTemporada.GetFocusedDataRow();
                DataRow drS = viewFavoritas.GetFocusedDataRow();
                if (dr != null)
                {
                    string id = dr["id"].ToString();
                    string sUrlRequest = "http://api.tvmaze.com/seasons/" + id + "/episodes";
                    var json = new WebClient().DownloadString(sUrlRequest);
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    var a = ser.Deserialize<List<Class13>>(json);

                    DataSet dsetEpisodiosVis = new DataSet();
                    dsetEpisodiosVis = serieBOL.CargarEpisodesVistos("1", drS["id"].ToString(), id);

                    dsetEpisodes = new DataSet();
                    DataTable dt = new DataTable();
                    dt.TableName = "episodes";
                    dt.Columns.Add("visto");
                    dt.Columns.Add("aux_bd");
                    dt.Columns.Add("id");
                    dt.Columns.Add("name");
                    dt.Columns.Add("season");
                    dt.Columns.Add("number");
                    dt.Columns.Add("image");
                    //
                    foreach (var item in a)
                    {
                        string ruta = "";
                        if (item.image != null)
                        {
                            ruta = item.image.original;
                        }
                        DataRow[] rows = dsetEpisodiosVis.Tables[0].Select("id_episode = "+item.id+"");
                        string visto = "N";
                        if (rows.Length > 0)
                        {
                            visto = "S";
                        }
                        dt.Rows.Add(visto, "N", item.id, item.name, item.season, item.number, ruta);
                    }
                    dsetEpisodes.Tables.Add(dt.Copy());
                    this.dtgCapitulos.DataSource = null;
                    this.dtgCapitulos.DataSource = dsetEpisodes.Tables[0];
                    //this.viewCapitulos.FocusedRowHandle = -1;
                }
                
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVisto_Click(object sender, EventArgs e)
        {
            try
            {
                int indice = this.viewCapitulos.FocusedRowHandle;
                this.viewCapitulos.FocusedRowHandle = -1;
                this.viewCapitulos.FocusedRowHandle = indice;
                if (this.viewCapitulos.FocusedRowHandle >= 0)
                    this.drSeleccionados = this.dsetEpisodes.Tables[0].Select("aux_bd = 'S'");
            }
            catch { }
            try
            {
                if (this.drSeleccionados.Length == 0)
                {
                    return;
                }
                DataRow dr = viewFavoritas.GetFocusedDataRow();
                DataRow dr2 = viewTemporada.GetFocusedDataRow();
                serieBOL.AgregarEpisodes(this.drSeleccionados, dr["id"].ToString(), dr2["id"].ToString());
                this.btnFiltrar_Click(null, null);
            }
            catch (Exception ex)
            {

            }
        }

        private void viewCapitulos_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            
        }

        private void viewCapitulos_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (this.viewCapitulos.FocusedRowHandle >= 0)
            {
                DataRow dr = viewCapitulos.GetFocusedDataRow();
                if (dr["visto"].ToString().Equals("S"))
                {
                    dr["visto"] = "S";
                }
                else
                {
                    dr["visto"] = "S";
                    dr["aux_bd"] = "S";
                }
            }
        }

        //
    }//
}//