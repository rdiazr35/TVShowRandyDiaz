﻿drop table favorites;
drop table usuario_serie;
drop table serie;

CREATE TABLE serie
(
	id integer NOT NULL,
	name text NOT NULL,
	language text NOT NULL,
	type text NOT NULL,
	image text NOT NULL,
	status text NOT NULL,
	rating text NOT NULL,
	CONSTRAINT pk_serie PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE usuario_serie
(
	id serial NOT NULL,
	name text NOT NULL,
	CONSTRAINT pk_user PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE favorites
(
	id_user integer NOT NULL,
	id_serie integer NOT NULL,
	CONSTRAINT pk_favorites PRIMARY KEY (id_user, id_serie),
	CONSTRAINT fk_favorites_user FOREIGN KEY (id_user)
		REFERENCES usuario_serie (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_favorites_serie FOREIGN KEY (id_serie)
		REFERENCES serie (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE TABLE episode
(
	id_user integer NOT NULL,
	id_serie integer NOT NULL,
	id_season integer NOT NULL,
	id_episode integer NOT NULL,
	CONSTRAINT pk_episode PRIMARY KEY (id_user, id_serie, id_season, id_episode)
)
WITH (
  OIDS=FALSE
);



select * from serie
select * from favorites

--INSERT INTO usuario_serie (name)values('RANDY');

