﻿namespace TVShowRandyDiaz
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlbarMenu = new System.Windows.Forms.ToolStrip();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBuscarSeriesPorNombre = new System.Windows.Forms.ToolStripButton();
            this.tlbarMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlbarMenu
            // 
            this.tlbarMenu.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.tlbarMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBuscarSeriesPorNombre});
            this.tlbarMenu.Location = new System.Drawing.Point(0, 0);
            this.tlbarMenu.Name = "tlbarMenu";
            this.tlbarMenu.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.tlbarMenu.Size = new System.Drawing.Size(928, 35);
            this.tlbarMenu.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::TVShowRandyDiaz.Properties.Resources.fondo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(928, 415);
            this.panel1.TabIndex = 7;
            // 
            // btnBuscarSeriesPorNombre
            // 
            this.btnBuscarSeriesPorNombre.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBuscarSeriesPorNombre.Image = global::TVShowRandyDiaz.Properties.Resources._22;
            this.btnBuscarSeriesPorNombre.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnBuscarSeriesPorNombre.Name = "btnBuscarSeriesPorNombre";
            this.btnBuscarSeriesPorNombre.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.btnBuscarSeriesPorNombre.Size = new System.Drawing.Size(42, 32);
            this.btnBuscarSeriesPorNombre.ToolTipText = "Buscar series por nombre";
            this.btnBuscarSeriesPorNombre.Click += new System.EventHandler(this.btnBuscarSeriesPorNombre_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tlbarMenu);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TvShow";
            this.tlbarMenu.ResumeLayout(false);
            this.tlbarMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tlbarMenu;
        private System.Windows.Forms.ToolStripButton btnBuscarSeriesPorNombre;
        private System.Windows.Forms.Panel panel1;
    }
}