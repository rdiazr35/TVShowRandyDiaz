﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvShowENL
{
    /// <summary>
    /// 
    /// </summary>
    public class Rootobject2
    {
        public Class12[] Property1 { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Class12
    {
        public int id { get; set; }
        public string url { get; set; }
        public int number { get; set; }
        public string name { get; set; }
        public int? episodeOrder { get; set; }
        public string premiereDate { get; set; }
        public string endDate { get; set; }
        public Network2 network { get; set; }
        public object webChannel { get; set; }
        public Image2 image { get; set; }
        public string summary { get; set; }
        public _Links2 _links { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Network2
    {
        public int id { get; set; }
        public string name { get; set; }
        public Country2 country { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Country2
    {
        public string name { get; set; }
        public string code { get; set; }
        public string timezone { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Image2
    {
        public string medium { get; set; }
        public string original { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class _Links2
    {
        public Self self { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Self2
    {
        public string href { get; set; }
    }


}
