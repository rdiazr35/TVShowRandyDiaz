﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvShowENL
{
    /// <summary>
    /// CLASES PARA EL CARGADO DE PARAMETROS DE EPISODIOS
    /// </summary>
    public class Rootobject3
    {
        public Class13[] Property1 { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Class13
    {
        public int? id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public int? season { get; set; }
        public int? number { get; set; }
        public string airdate { get; set; }
        public string airtime { get; set; }
        public DateTime airstamp { get; set; }
        public int? runtime { get; set; }
        public Image3 image { get; set; }
        public string summary { get; set; }
        public _Links3 _links { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Image3
    {
        public string medium { get; set; }
        public string original { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class _Links3
    {
        public Self self { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Self3
    {
        public string href { get; set; }
    }
}
