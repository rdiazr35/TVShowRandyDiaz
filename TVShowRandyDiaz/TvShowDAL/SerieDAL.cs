﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvShowDAL
{
    public class SerieDAL
    {
        /// <summary>
        /// CARGADO DE FAVORIROS
        /// </summary>
        /// <param name="id_usuario"></param>
        /// <returns></returns>
        public DataSet CargarFavoritos(string id_usuario)
        {
            StringBuilder sql = new StringBuilder();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                DataSet dsetDatos = new DataSet();
                NpgsqlDataAdapter reader;
                con.Open();
                sql.AppendLine(" SELECT ");
                sql.AppendLine("    id_user, id_serie, s.*   ");
                sql.AppendLine(" FROM ");
                sql.AppendLine("    favorites f ");
                sql.AppendLine("    LEFT OUTER JOIN serie s on (f.id_serie = s.id) ");
                sql.AppendLine("WHERE ");
                sql.AppendLine("    id_user = " + id_usuario + " ");
                sql.AppendLine("ORDER BY ");
                sql.AppendLine("    name ");

                NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), con);
                //NpgsqlDataReader reader = cmd.ExecuteReader();
                reader = new NpgsqlDataAdapter(cmd);
                try
                {
                    reader.Fill(dsetDatos, "favoritas");
                    cmd.Dispose();
                    reader.Dispose();
                }
                catch (Exception error)
                {
                    return null;
                }
                return dsetDatos;
            }
        }
        /// <summary>
        /// AGREGAR EPISODIOS
        /// </summary>
        /// <param name="id_user"></param>
        /// <param name="id_season"></param>
        /// <param name="id_serie"></param>
        /// <param name="id_episode"></param>
        public void AgregarEpisodes(string id_user, string id_season, string id_serie, string id_episode)
        {
            StringBuilder sql = new StringBuilder();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                DataSet dsetDatos = new DataSet();
                con.Open();
                sql.AppendLine(" INSERT INTO episode (id_user, id_serie, id_season, id_episode) ");
                sql.AppendLine("    VALUES (" + id_user + ", " + id_season + ", " + id_serie + ", " + id_episode + " ); ");

                NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), con);
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// CARGADO DE EPISODIOS VISTOS
        /// </summary>
        /// <param name="id_user"></param>
        /// <param name="id_serie"></param>
        /// <param name="id_season"></param>
        /// <returns></returns>
        public DataSet CargarEpisodesVistos(string id_user, string id_serie, string id_season)
        {
            StringBuilder sql = new StringBuilder();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                DataSet dsetDatos = new DataSet();
                NpgsqlDataAdapter reader;
                con.Open();
                sql.AppendLine(" SELECT ");
                sql.AppendLine("    *   ");
                sql.AppendLine(" FROM ");
                sql.AppendLine("    episode ");
                sql.AppendLine("WHERE ");
                sql.AppendLine("    id_user = " + id_user + " ");
                sql.AppendLine("AND ");
                sql.AppendLine("    id_serie = " + id_serie + " ");
                sql.AppendLine("AND ");
                sql.AppendLine("    id_season = " + id_season + " ");

                NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), con);
                //NpgsqlDataReader reader = cmd.ExecuteReader();
                reader = new NpgsqlDataAdapter(cmd);
                try
                {
                    reader.Fill(dsetDatos, "episodios");
                    cmd.Dispose();
                    reader.Dispose();
                }
                catch (Exception error)
                {
                    return null;
                }
                return dsetDatos;
            }
        }
        /// <summary>
        /// AGREGAR SERIES
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="language"></param>
        /// <param name="type"></param>
        /// <param name="image"></param>
        /// <param name="status"></param>
        /// <param name="rating"></param>
        public void Agregar(string id, string name, string language, string type, string image, string status, string rating)
        {
            StringBuilder sql = new StringBuilder();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                DataSet dsetDatos = new DataSet();
                con.Open();
                sql.AppendLine(" INSERT INTO serie (id, name, language, type, image, status, rating) ");
                sql.AppendLine("    VALUES ("+ id+", '"+name+"', '"+language+"', '"+type+"', '"+image+"', '"+status+"', '"+rating+"' ); ");
                sql.AppendLine(" INSERT INTO favorites (id_user, id_serie) ");
                sql.AppendLine("    VALUES (1, " + id + " ); ");

                NpgsqlCommand cmd = new NpgsqlCommand(sql.ToString(), con);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
