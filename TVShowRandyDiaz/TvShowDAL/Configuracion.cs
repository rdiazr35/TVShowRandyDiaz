﻿using System.Configuration;

namespace TvShowDAL
{
    public static class Configuracion
    {
        /// <summary>
        /// String de conexion a postgresql
        /// </summary>
        private static string conStr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
        /// <summary>
        /// 
        /// </summary>
        public static string ConStr
        {
            get { return conStr; }
        }

    }//END CLASS
}//END NAMESPACE